<Query Kind="Program" />

void Main()
{

    _stop = new Stopwatch();
    var random = new Random();

    var values = new List<int>();

    for(int i = 0 ; i < 200000; i++)
    {
        values.Add(random.Next());
    }
    var fasterArray = 0;
    var fasterMax = 0;
	double avgArray = 0;
	double avgMax = 0;
	int amountOfTries = 1000;
    for(int i = 0; i < amountOfTries; i++)
    {
        Start();
        var max = values.Max((x) => x);
        var ticksMax = Stop("values.Max");
        var array = values.ToArray();
        Start();
        var maxx = Max(array);
        var ticksArray = Stop("Max(array)");
		
		avgArray += ticksArray;
		avgMax += ticksMax;
		

        if(ticksMax > ticksArray)
        {
            fasterArray++;
        }
        else if (ticksMax == ticksArray)
        {
            fasterArray++;
            fasterMax++;
        }
        else
        {
            fasterMax++;
        }
    }
	
	Console.WriteLine($"fasterArray {fasterArray} Average MS {avgArray/amountOfTries} {Environment.NewLine}, fasterMax {fasterMax} Average MS {avgMax/amountOfTries}");
}

Stopwatch _stop;

public void Start()
{
    _stop.Restart();
}

public double Stop(string foo)
{
    _stop.Stop();
    //Console.WriteLine($"{_stop.ElapsedMilliseconds}MS {foo}");
    return _stop.Elapsed.TotalMilliseconds;
}


public static int Max(params int[] values)
{
    var maximum = int.MinValue;
    foreach(var value in values)
    {
        maximum = Math.Max(maximum, value);
    }

    return maximum;
}
