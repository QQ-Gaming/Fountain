<Query Kind="Program" />

void Main()
{
    var cubes = new []
    {
        new CubeCoordinates(1, 0, -1), new CubeCoordinates(1, 1, -2), new CubeCoordinates(0, 0, 0), new CubeCoordinates(3, 4, -7),
        new CubeCoordinates(0, 0, 0)
    };


    CubeCoordinates.CalculateLine(cubes[0], cubes[3]).Dump();

    foreach(var cube in cubes)
    {
        Console.WriteLine(cube.GetHashCode());
    }

}

public class CubeCoordinates
{
    public int X, Y, Z;

    public CubeCoordinates(int x, int y, int z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    public static FractionalCubeCoordinates Lerp(CubeCoordinates a, CubeCoordinates b, float t)
    {
        var fractionalCube = new FractionalCubeCoordinates(MathF.Lerp(a.X, b.X, t), MathF.Lerp(a.Y, b.Y, t), MathF.Lerp(a.Z, b.Z, t));
        return fractionalCube;
    }

    public static CubeCoordinates[] CalculateLine(CubeCoordinates from, CubeCoordinates to)
    {
        var distance = Distance(from, to);

        var cubes = new CubeCoordinates[distance + 1];

        for(int i = 0; i < cubes.Length; i++)
        {
            cubes[i] = Round(Lerp(from, to, 1.0f / distance * i));
        }
        return cubes;
    }

    public static int Distance(CubeCoordinates a, CubeCoordinates b)
    {
        return MathF.Max(Math.Abs(a.X - b.X), Math.Abs(a.Y - b.Y), Math.Abs(a.Z - b.Z));
    }

    public static CubeCoordinates Round(FractionalCubeCoordinates a)
    {
        var roundedX = MathF.Round(a.X);
        var roundedY = MathF.Round(a.Y);
        var roundedZ = MathF.Round(a.Z);

        var xDifference = Math.Abs(roundedX - a.X);
        var yDifference = Math.Abs(roundedY - a.Y);
        var zDifference = Math.Abs(roundedZ - a.Z);

        if(xDifference > yDifference && xDifference > zDifference)
        {
            roundedX = -roundedY - roundedZ;
        }
        else if(yDifference > zDifference)
        {
            roundedY = -roundedX - roundedZ;
        }
        else
        {
            roundedZ = -roundedX - roundedY;
        }
        return new CubeCoordinates(roundedX, roundedY, roundedZ);
    }

    public override int GetHashCode()
    {
        var hashCode = -307843816;
        hashCode = hashCode * -1521134295 + X.GetHashCode();
        hashCode = hashCode * -1521134295 + Y.GetHashCode();
        hashCode = hashCode * -1521134295 + Z.GetHashCode();
        return hashCode;
    }
}

public class PointF
{
    public float X, Y;

    public PointF(float x, float y)
    {
        X = x;
        Y = y;
    }
}

public class FractionalCubeCoordinates
{
    public double X, Y, Z;

    public FractionalCubeCoordinates(double x, double y, double z)
    {
        X = x;
        Y = y;
        Z = z;
    }
}

public static class MathF
{

    public static double Lerp(float a, float b, float t)
    {
        return a + (b - a) * t;
    }

    public static int Round(double value)
    {
        return (int) Math.Round(value);
    }

    public static int Max(int a, int b, int c)
    {

        var maxAB = Math.Max(a, b);
        var maxABC = Math.Max(maxAB, c);

        return maxABC;
    }
}