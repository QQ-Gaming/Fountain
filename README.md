# Fountain

The goal of this library is to define useful mathmatical operations for hexagons. Later this library will be used to make random map generation with noises.

# Old Examples

What can be done with this hexagonal library. Those gifs show our in 2016 created hexagon "library", which has been implemented in a gaming prototype of us. When this project is in a state where it's possible to create maps with it, we'll extend those gifs with new content.

<img src="Examples/hexagon.gif" width="200">
<img src="Examples/triangle.gif" width="200">

# Examples

The following section tracks the current process of the Fountain.Hexagon library and shows examples what kind of stuff is possible to create, in each testing platform.

## Universal Windows Application

<img src="Examples/UWP/hexagon maps.png" width="600">

<img src="Examples/UWP/parallelogram maps.png" width="600">

<img src="Examples/UWP/triangle maps.png" width="600">

<img src="Examples/UWP/randomhexes.png" width="600">


# Goals

For the first version all of those points below should be implemented. 

* cube coordinates
* axial coordinates
* flat hexagons 
* pointy hexagons
* neighbors
* distance
* range
* obstacles
* rings
* pix to hex
* hex to pix
* nearest hex
* pathfinding
* random map generation (Noise)