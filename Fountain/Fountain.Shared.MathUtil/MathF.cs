﻿using System;
using System.Diagnostics;

namespace Fountain.Shared.MathUtil
{
    public static class MathF
    {
        public static double Lerp(float a,float b,float t)
        {
            return a + (b - a) * t;
        }

        public static int Round(double value)
        {
           return (int) Math.Round(value);
        }

        public static int Max(params int[] values)
        {
            var maximum = int.MinValue;
            foreach(var value in values)
            {
                maximum = Math.Max(maximum, value);
            }
            
            return maximum;
        }
    }
}
