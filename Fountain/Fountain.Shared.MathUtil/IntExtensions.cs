﻿using System;

namespace Fountain.Shared.MathUtil
{
    public static class IntExtensions
    {
        public static float ToRadiant(this int degree)
        {
            return (float)(Math.PI / 180) * degree;
        }
    }
}
