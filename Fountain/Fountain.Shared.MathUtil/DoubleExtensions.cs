﻿namespace Fountain.Shared.MathUtil
{
    public static class DoubleExtensions
    {
        public static float ToFloat(this double value)
        {
            return (float)value;
        }
    }
}
