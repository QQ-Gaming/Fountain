﻿using System.Drawing;

namespace Fountain.Shared.MathUtil
{
    public static class PointFExtensions
    {
        public static PointF Add(this PointF point, PointF other)
        {
            return new PointF(point.X + other.X, point.Y + other.Y);
        }
    }
}
