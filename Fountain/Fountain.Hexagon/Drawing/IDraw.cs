﻿using Fountain.Hexagon.Coordinates;
using System.Collections.Generic;

namespace Fountain.Hexagon.Drawing
{
    public interface IDraw<T>
    {
        T Draw(Hex hex);
        IList<T> Draw(IDictionary<CubeCoordinates, Hex> map);
    }
}
