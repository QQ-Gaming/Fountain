﻿using Fountain.Hexagon.Coordinates;
using Fountain.Hexagon.Geometry;
using System;
using System.Collections.Generic;

namespace Fountain.Hexagon.Maps
{
    internal static class HexagonMap
    {

        public static Dictionary<CubeCoordinates, Hex> Create(int radius, Layout layout)
        {
            var map = new Dictionary<CubeCoordinates, Hex>();

            for (int q = -radius; q <= radius; q++)
            {
                int rStart = Math.Max(-radius, -q - radius);
                int rEnd = Math.Min(radius, -q + radius);

                for (int r = rStart; r <= rEnd; r++)
                {
                    var cube = new CubeCoordinates(q, r, -q - r);
                    map.Add(cube, new Hex(cube, layout));
                }
            }

            return map;
        }
    }
}
