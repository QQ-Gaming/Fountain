﻿using Fountain.Hexagon.Coordinates;
using Fountain.Hexagon.Geometry;
using System.Collections.Generic;
using System.Drawing;

namespace Fountain.Hexagon.Maps
{
    internal static class ParallelogramMap
    {
        public static Dictionary<CubeCoordinates, Hex> Create(PointF amount, Layout layout)
        {
            var map = new Dictionary<CubeCoordinates, Hex>();

            for (int q = 0; q < amount.X; q++)
            {
                for (int r = 0; r < amount.Y; r++)
                {
                    var cube = FromOrientation(q, r, layout.Orientation);
                    var hex = new Hex(cube, layout);
                    map.Add(cube, hex);
                }
            }

            return map;
        }

        private static CubeCoordinates FromOrientation(int q, int r, Orientation orientation)
        {
            switch (orientation)
            {
                case Orientation.Default:
                case Orientation.Left:
                    return new CubeCoordinates(q, r, -q - r);
                case Orientation.Right:
                    return new CubeCoordinates(-q - r, q, r);
                case Orientation.Up:
                case Orientation.Down:
                    return new CubeCoordinates(r, -q - r, q);
                default:
                    return new CubeCoordinates(q, r, -q - r);
            }
        }
    }
}
