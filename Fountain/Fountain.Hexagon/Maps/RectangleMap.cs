﻿using Fountain.Hexagon.Coordinates;
using Fountain.Hexagon.Geometry;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Fountain.Hexagon.Maps
{
    internal static class RectangleMap
    {
        public static Dictionary<CubeCoordinates, Hex> Create(PointF size, Layout layout)
        {
            var map = new Dictionary<CubeCoordinates, Hex>();

            for (int r = 0; r < size.Y; r++)
            {
                var rOffset = (int)Math.Floor((decimal)(r / 2));

                for (int q = -rOffset; q < size.X - rOffset; q++)
                {
                    var cube = new CubeCoordinates(q, r, -q - r);
                    map.Add(cube, new Hex(cube, layout));
                }
            }
            return map;
        }
    }
}
