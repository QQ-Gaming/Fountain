﻿using Fountain.Hexagon.Coordinates;
using Fountain.Hexagon.Geometry;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Fountain.Hexagon.Maps
{
    public class Map : IMap
    {
        /// <summary>
        /// Creates a map which has the shape of the given map type.
        /// </summary>
        /// <param name="map">defines the shape of the map</param>
        /// <param name="fieldAmount">The field amount has different behavior, for different map types.
        /// f.e. a triangle map with a shape of hexagons, just uses the X variable of the fieldAmount variable.
        /// A rectangle map, uses both the X/Y variable for the size of the map.</param>
        /// <param name="layout"></param>
        /// <returns></returns>
        public Dictionary<CubeCoordinates, Hex> Create(MapType map, PointF fieldAmount, Layout layout)
        {
            return CreateHexagonMap(map, fieldAmount, layout);
        }

        private Dictionary<CubeCoordinates, Hex> CreateHexagonMap(MapType map, PointF size, Layout layout)
        {
            var dictionary = new Dictionary<CubeCoordinates, Hex>();

            switch (map)
            {
                case MapType.Parallelogram:
                    dictionary = ParallelogramMap.Create(size, layout);
                    break;
                case MapType.Triangle:
                    dictionary = TriangleMap.Create((int)size.X, layout);
                    break;
                case MapType.Hexagon:
                    dictionary = HexagonMap.Create((int)size.X, layout);
                    break;
                case MapType.Rectangle:
                    dictionary = RectangleMap.Create(size, layout);
                    break;
                default:
                    throw new ArgumentException(nameof(size));
            }

            return dictionary;
        }
    }

    interface IMap
    {
        Dictionary<CubeCoordinates, Hex> Create(MapType map, PointF size, Layout layout);
    }
}
