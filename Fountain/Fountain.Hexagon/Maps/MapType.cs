﻿namespace Fountain.Hexagon.Maps
{
    public enum MapType
    {
        Parallelogram,
        Triangle,
        Hexagon,
        Rectangle,
    }

    public enum ShapeType
    {
        Hexagon
    }

    public enum Orientation
    {
        Default,
        Left,
        Right,
        Up,
        Down
    }
}
