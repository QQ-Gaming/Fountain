﻿using Fountain.Hexagon.Coordinates;
using Fountain.Hexagon.Geometry;
using System.Collections.Generic;

namespace Fountain.Hexagon.Maps
{
    internal static class TriangleMap
    {
        public static Dictionary<CubeCoordinates, Hex> Create(int size, Layout layout)
        {
            return CreateFromOrientation(size, layout);
        }

        private static Dictionary<CubeCoordinates, Hex> CreateFromOrientation(int size, Layout layout)
        {
            switch (layout.Orientation)
            {
                case Orientation.Default:
                case Orientation.Right:
                case Orientation.Down:
                default:
                    return CreateRightDown(size, layout);
                case Orientation.Left:
                case Orientation.Up:
                    return CreateLeftUp(size, layout);
            }
        }

        private static Dictionary<CubeCoordinates, Hex> CreateRightDown(int size, Layout layout)
        {
            var map = new Dictionary<CubeCoordinates, Hex>();

            for (int q = 0; q < size; q++)
            {
                for (int r = 0; r < size - q; r++)
                {
                    var cube = new CubeCoordinates(q, r, -q - r);
                    map.Add(cube, new Hex(cube, layout));
                }
            }
            return map;
        }

        private static Dictionary<CubeCoordinates, Hex> CreateLeftUp(int size, Layout layout)
        {
            var map = new Dictionary<CubeCoordinates, Hex>();

            for (int q = 0; q < size; q++)
            {
                for (int r = size - q; r < size; r++)
                {
                    var cube = new CubeCoordinates(q, r, -q - r);
                    map.Add(cube, new Hex(cube, layout));
                }
            }
            return map;
        }

    }
}
