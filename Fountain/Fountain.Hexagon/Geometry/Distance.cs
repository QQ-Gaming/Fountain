﻿using Fountain.Hexagon.Coordinates;
using System;

namespace Fountain.Hexagon.Geometry
{
    internal static class Distance
    {
        internal static int Calculate(CubeCoordinates a, CubeCoordinates b)
        {
            return Length(a - b);
        }

        private static int Length(CubeCoordinates coordinates)
        {
            return (Math.Abs(coordinates.X) + Math.Abs(coordinates.Y) + Math.Abs(coordinates.Z)) / 2;
        }
    }
}
