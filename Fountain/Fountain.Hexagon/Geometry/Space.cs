﻿using Fountain.Shared.MathUtil;
using System;
using System.Drawing;

namespace Fountain.Hexagon.Geometry
{
    internal static class Space
    {
        internal static PointF Flat(PointF size)
        {
            var height = Math.Sqrt(3) * size.Y;
            var width = 2 * size.X;

            return new PointF(0.75f * width, height.ToFloat());
        }

        internal static PointF Pointy(PointF size)
        {
            var height = 2 * size.Y;
            var width = Math.Sqrt(3) * size.X;

            return new PointF(width.ToFloat(), 0.75f * height);
        }
    }
}
