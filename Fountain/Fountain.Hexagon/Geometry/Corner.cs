﻿using Fountain.Shared.MathUtil;
using System;
using System.Drawing;
using Fountain.Shared.ExceptionUtil;

namespace Fountain.Hexagon.Geometry
{
    internal static class Corner
    {

        internal static PointF[] Create(PointF center, Layout layout)
        {
            var points = new PointF[6];

            for (int i = 0; i < 6; i++)
            {
                points[i] = CreateCorner(layout.Type, center, layout.Size, i);
            }

            return points;
        }

        private static PointF CreateCorner(HexType type, PointF center, PointF size, int corner)
        {
            switch (type)
            {
                case HexType.Pointy:
                    return Pointy(center, size, corner);
                case HexType.Flat:
                    return Flat(center, size, corner);
                default:
                    throw new ArgumentException($"unknown type {type}");
            }
        }

        internal static PointF Pointy(PointF center, PointF size, int corner)
        {
            size.Ensure(size.X > 0);
            size.Ensure(size.Y > 0);
            corner.Ensure(corner >= 0 && corner < 6);

            var degree = (60 * corner) - 30;
            var radiant = degree.ToRadiant();

            return CalculateCorner(center, size, radiant);
        }

        /// <summary>
        /// returns the corner position of a hexagon in a flat topped orientation
        /// C4      ------      C5
        ///        -      -
        /// C3    -        -    C0
        ///        -      -
        /// C2      ------      C1
        /// </summary>
        /// <param name="center"></param>
        /// <param name="size"></param>
        /// <param name="corner"></param>
        /// <returns>C0...C5 based on the corner parameter</returns>
        internal static PointF Flat(PointF center, PointF size, int corner)
        {
            size.Ensure(size.X > 0);
            size.Ensure(size.Y > 0);
            corner.Ensure(corner >= 0 && corner < 6);

            var degree = 60 * corner;
            var radiant = degree.ToRadiant();

            return CalculateCorner(center, size, radiant);
        }

        private static PointF CalculateCorner(PointF center, PointF size, float radiant)
        {
            return new PointF((center.X + size.X * Math.Cos(radiant)).ToFloat(), (center.Y + size.Y * Math.Sin(radiant)).ToFloat());
        }
    }
}
