﻿using Fountain.Hexagon.Coordinates;
using Fountain.Shared.ExceptionUtil;
using System;

namespace Fountain.Hexagon.Geometry
{
    internal static class Neighbors
    {
        private static CubeCoordinates[] _directions = new[]
        {
            new CubeCoordinates(1,0,-1),
            new CubeCoordinates(1,-1,0),
            new CubeCoordinates(0,-1,1),
            new CubeCoordinates(-1,0,1),
            new CubeCoordinates(-1,1,0),
            new CubeCoordinates(0,1,-1)
        };

        internal static CubeCoordinates GetNeighbor(CubeCoordinates coords, int direction)
        {
            return coords + Direction(direction);
        }

        internal static CubeCoordinates[] GetNeighbors(CubeCoordinates coords)
        {
            var cubes = new CubeCoordinates[_directions.Length];
            for(int i = 0; i<_directions.Length; i++)
            {
                cubes[i] = GetNeighbor(coords, i);
            }

            return cubes;
        }

        internal static CubeCoordinates[] GetNeighborsWithCenter(CubeCoordinates center)
        {
            var cubes = new CubeCoordinates[_directions.Length + 1];
            var neighors = GetNeighbors(center);
            Array.Copy(neighors, cubes, neighors.Length);
            cubes[_directions.Length] = center;
            return cubes;
        }

        private static CubeCoordinates Direction(int direction)
        {
            direction.Ensure(
                0 <= direction && direction < _directions.Length,
                new ArgumentException($"direction has to be between 0 and {_directions.Length}"));

            return _directions[direction];
        }
    }
}
