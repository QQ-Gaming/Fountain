﻿using Fountain.Hexagon.Maps;
using System.Drawing;

namespace Fountain.Hexagon.Geometry
{
    public struct Layout
    {
        public PointF Size { get; set; }
        public PointF Origin { get; set; }
        public HexType Type { get; set; }
        public Orientation Orientation { get; set; }

    }
}
