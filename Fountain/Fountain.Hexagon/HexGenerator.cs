﻿using Fountain.Hexagon.Geometry;
using System;
using System.Collections.Generic;

namespace Fountain.Hexagon
{
    public static class HexGenerator
    {
        private static Random _rand = new Random();

        public static IEnumerable<Hex> GenerateHexes(int amount, Func<Layout> generateLayout)
        {
            for (int i = 0; i < amount; i++)
            {
                var randX = _rand.Next(0, 1400);
                var randY = _rand.Next(0, 1400);
                var layout = generateLayout.Invoke();
                yield return new Hex(
                    new System.Drawing.PointF(randX, randY), layout);
            }
        }
    }
}
