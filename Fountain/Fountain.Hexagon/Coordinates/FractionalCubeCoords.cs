﻿using Fountain.Shared.ExceptionUtil;
using Fountain.Shared.MathUtil;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Hexagon.Test")]

namespace Fountain.Hexagon.Coordinates
{
    internal struct FractionalCubeCoords
    {
        public double X { get; }
        public double Y { get; }
        public double Z { get; }

        public FractionalCubeCoords(double x = 0, double y = 0, double z = 0, bool roundToNearestZeroSum = false)
        {
            if (roundToNearestZeroSum && (x + y + z != 0))
            {
                var a = Round(x, y, z);

                x = a.Item1;
                y = a.Item2;
                z = a.Item3;
            }

            x.Ensure(x + y + z == 0);

            X = x;
            Y = y;
            Z = z;
        }

        public static FractionalCubeCoords Lerp(CubeCoordinates a, CubeCoordinates b, float t)
        {
            var fractionalCube = new FractionalCubeCoords(MathF.Lerp(a.X, b.X, t), MathF.Lerp(a.Y, b.Y, t), MathF.Lerp(a.Z, b.Z, t));
            return fractionalCube;
        }

        private static Tuple<double, double, double> Round(double x, double y, double z)
        {
            var xRounded = MathF.Round(x);
            var yRounded = MathF.Round(y);
            var zRounded = MathF.Round(y);

            var xDifference = Math.Abs(xRounded - x);
            var yDifference = Math.Abs(yRounded - z);
            var zDifference = Math.Abs(zRounded - z);

            if (xDifference > yDifference && xDifference > zDifference)
            {
                x = -y - z;
            }
            else if (yDifference > zDifference)
            {
                y = -x - z;
            }
            else
            {
                z = -x - y;
            }
            return new Tuple<double, double, double>(x, y, z);
        }
    }
    internal static class FractionalCubeCoordsExtensions
    {      
        public static CubeCoordinates Round(this FractionalCubeCoords a)
        {
            var roundedX = MathF.Round(a.X);
            var roundedY = MathF.Round(a.Y);
            var roundedZ = MathF.Round(a.Z);

            var xDifference = Math.Abs(roundedX - a.X);
            var yDifference = Math.Abs(roundedY - a.Y);
            var zDifference = Math.Abs(roundedZ - a.Z);

            if (xDifference > yDifference && xDifference > zDifference)
            {
                roundedX = -roundedY - roundedZ;
            }
            else if (yDifference > zDifference)
            {
                roundedY = -roundedX - roundedZ;
            }
            else
            {
                roundedZ = -roundedX - roundedY;
            }
            return new CubeCoordinates(roundedX, roundedY, roundedZ);
        }
    }
}
