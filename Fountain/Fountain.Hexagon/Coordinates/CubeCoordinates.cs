﻿using Fountain.Hexagon.Geometry;
using Fountain.Shared.ExceptionUtil;
using Fountain.Shared.MathUtil;
using System;
using System.Drawing;
using static Fountain.Shared.MathUtil.MathF;

namespace Fountain.Hexagon.Coordinates
{
    /// <summary>
    /// Define a position from a hexagon inside a grid.
    /// </summary>
    public struct CubeCoordinates
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public CubeCoordinates(int x = 0, int y = 0, int z = 0)
        {
            x.Ensure(x + y + z == 0);

            X = x;
            Y = y;
            Z = z;
        }

        public static int Distance(CubeCoordinates a, CubeCoordinates b)
        {
            return Max(Math.Abs(a.X - b.X), Math.Abs(a.Y - b.Y), Math.Abs(a.Z - b.Z));
        }

        public static CubeCoordinates[] CalculateLine(CubeCoordinates from, CubeCoordinates to)
        {
            var distance = Distance(from, to);

            var cubes = new CubeCoordinates[distance + 1];

            for (int i = 0; i < cubes.Length; i++)
            {
                cubes[i] = FractionalCubeCoords.Lerp(from, to, 1.0f / distance * i).Round();
            }
            return cubes;
        }

        public static CubeCoordinates operator +(CubeCoordinates a, CubeCoordinates b) => new CubeCoordinates(a.X + b.X, a.Y + b.Y, a.Z + b.Z);

        public static CubeCoordinates operator -(CubeCoordinates a, CubeCoordinates b) => new CubeCoordinates(a.X - b.X, a.Y - b.Y, a.Z - b.Z);

        public static CubeCoordinates operator *(CubeCoordinates a, CubeCoordinates b) => new CubeCoordinates(a.X * b.X, a.Y * b.Y, a.Z * b.Z);

        public static CubeCoordinates operator *(CubeCoordinates a, int b) => new CubeCoordinates(a.X * b, a.Y * b, a.Z * b);

        public static CubeCoordinates operator /(CubeCoordinates a, CubeCoordinates b) => new CubeCoordinates(a.X / b.X, a.Y / b.Y, a.Z / b.Z);

        public static bool operator ==(CubeCoordinates a, CubeCoordinates b) => (a.X == b.X) && (a.Y == b.Y) && (a.Z == b.Z);

        public static bool operator !=(CubeCoordinates a, CubeCoordinates b) => (a.X != b.X) || (a.Y != b.Y) || (a.Z != b.Z);

        public override bool Equals(object obj)
        {
            if (!(obj is CubeCoordinates coordinates))
            {
                return false;
            }

            return X == coordinates.X &&
                   Y == coordinates.Y &&
                   Z == coordinates.Z;
        }

        public override int GetHashCode()
        {
            var hashCode = -307843816;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            hashCode = hashCode * -1521134295 + Z.GetHashCode();
            return hashCode;
        }
    }

    public static class CubeExtensions
    {
        public static PointF ToPoint(this CubeCoordinates cube, Layout layout)
        {
            if (layout.Type == HexType.Flat)
            {
                var point = Space.Flat(layout.Size);
                point.X *= cube.X;
                point.Y *= (cube.Y + 0.5f * cube.X);
                return point;
            }
            else
            {
                var pointy = Space.Pointy(layout.Size);
                pointy.X *= (cube.X + 0.5f * cube.Y);
                pointy.Y *= cube.Y;
                return pointy;
            }
        }
    }
}
