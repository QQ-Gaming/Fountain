﻿namespace Fountain.Hexagon.Coordinates
{
    /// <summary>
    /// Like <see cref="CubeCoordinates"/> only different is the last value,
    /// which we call S. S is calculated on the fly using the constraint
    /// of the <see cref="CubeCoordinates"/>
    /// S = -Q -R
    /// </summary>
    public struct AxialCoordinates
    {
        public int Q, R;

        /// <summary>
        /// S is calculated on demand using the other two values
        /// S = -Q - R
        /// </summary>
        public int S { get => -Q - R; }

        public AxialCoordinates(int q, int r)
        {
            Q = q;
            R = r;
        }
    }
}
