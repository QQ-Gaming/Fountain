﻿using Fountain.Hexagon.Coordinates;
using Fountain.Hexagon.Geometry;
using Fountain.Shared.MathUtil;
using System;
using System.Drawing;

namespace Fountain.Hexagon
{
    public class Hex
    {
        public PointF[] CornerPoints { get; private set; }

        public Color Fill { get; set; }

        public Color Border { get; set; }

        internal CubeCoordinates Coordinates { get; private set; }

        public PointF Center { get; private set; }

        public Hex(PointF center, Layout layout)
        {
            CornerPoints = Corner.Create(center, layout);
        }

        public CubeCoordinates[] Line(Hex to)
        {
          return CubeCoordinates.CalculateLine(Coordinates, to.Coordinates);
        }
        

        public override string ToString()
        {
            return $"[{Coordinates.X},{Coordinates.Y},{Coordinates.Z}]";
        }

        internal Hex(CubeCoordinates coordinates, Layout layout)
        {
            Coordinates = coordinates;
            Center = coordinates.ToPoint(layout).Add(layout.Origin);
            CornerPoints = Corner.Create(Center, layout);
        }

    }
}
