﻿using Fountain.Hexagon.Coordinates;
using Fountain.Hexagon.Geometry;
using Fountain.Shared.ExceptionUtil;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Hexagon.Test")]
namespace Fountain.Hexagon
{
    internal static class Ring
    {
        public static IEnumerable<CubeCoordinates> Single(CubeCoordinates center, int radius)
        {
            radius.Ensure(radius >= 0);

            if (radius == 0) { return new[] { center }; }

            var cube = center + Neighbors.GetNeighbor(center, 4) * radius;
            var cubes = new List<CubeCoordinates>();

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < radius; j++)
                {
                    cubes.Add(cube);
                    cube = Neighbors.GetNeighbor(cube, i);
                }
            }
            return cubes;
        }

        public static IEnumerable<CubeCoordinates> Spiral(CubeCoordinates center, int radius)
        {
            var cubes = new List<CubeCoordinates>();
            for (int innerRadius = 0; innerRadius <= radius; innerRadius++)
            {
                cubes.AddRange(Single(center, innerRadius));
            }
            return cubes;
        }
    }
}
