﻿
using Fountain.Hexagon;
using Fountain.Hexagon.Coordinates;
using System;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class MapDrawer  {
        
    internal List<Mesh> Draw(Dictionary<CubeCoordinates,Hex> map, Func<UnityEngine.Color> generateColor)
    {
        var meshes = new List<Mesh>();

        foreach(var hex in map)
        {
            meshes.Add(GenerateMesh(hex.Value,generateColor));
        }

        return meshes;
    }

    private Mesh GenerateMesh(Hex hex,Func<UnityEngine.Color> generateColor)
    {
        var vectors = CalculateVectors(hex);

        var vector3ds = Array.ConvertAll<Vector2, Vector3>(vectors.ToArray(), v => v);

        var triangulator = new Triangulator(vectors.ToArray());

        var colors = new UnityEngine.Color[vector3ds.Length];

        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = generateColor();
        }

        var mesh = new Mesh()
        {
            vertices = vector3ds,
            triangles = triangulator.Triangulate(),
            colors = colors
        };

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        return mesh;
    }

    private List<Vector2> CalculateVectors(Hex hex)
    {
        var vectors = new List<Vector2>();
        foreach(var corner in hex.CornerPoints)
        {
            vectors.Add(From(corner));
        }

        return vectors;
    }

    private Vector2 From(PointF point)
    {
        return new Vector3(point.X,point.Y);
    }
}
