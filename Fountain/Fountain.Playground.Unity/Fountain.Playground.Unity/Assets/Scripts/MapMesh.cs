﻿using Fountain.Hexagon;
using Fountain.Hexagon.Geometry;
using Fountain.Hexagon.Maps;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

/// <summary>
/// This class draws a map in unity.
/// The way the map is drawn is not the best.
/// </summary>
public class MapMesh : MonoBehaviour
{
    private Map _mapFactory = new Map();
    private MapDrawer _drawer = new MapDrawer();

    void Start()
    {

        var triangleMap = CreateTriangleMap(_drawer, _mapFactory);

        var triangleParent = new GameObject("TriangleMap");
        CreateGameObjects(triangleParent, triangleMap);

        var parallelogramMap = CreateParallelogramMap(_drawer,_mapFactory);
        var parallelogramParent = new GameObject("ParallelogramMap");
        CreateGameObjects(parallelogramParent, parallelogramMap);


        var rectangleMap = CreateRectangleMap(_drawer, _mapFactory);
        var rectangleParent = new GameObject("RectangleMap");
        CreateGameObjects(rectangleParent, rectangleMap);


        var hexagonMap = CreateHexagonMap(_drawer, _mapFactory);
        var hexagonParent = new GameObject("HexagonMap");
        CreateGameObjects(hexagonParent, hexagonMap);
    }

    private List<Mesh> CreateParallelogramMap(MapDrawer drawer,Map mapFactory)
    {
        var parallelogram = mapFactory.Create(MapType.Parallelogram, new PointF(5,5),
            new Layout()
            {
                Orientation = Orientation.Down,
                Origin = new PointF(300, 300),
                Size = new PointF(2, 2)
           ,
                Type = HexType.Pointy
            });

       return drawer.Draw(parallelogram, () => UnityEngine.Color.red);
    }

    private static List<Mesh> CreateTriangleMap(MapDrawer drawer, Map mapFactory)
    {
        var triangle = mapFactory.Create(MapType.Triangle, new PointF(10, 0),
            new Layout()
            {
                Orientation = Orientation.Down,
                Origin = new PointF(10, 10),
                Size = new PointF(2, 2)
           ,
                Type = HexType.Pointy
            });

        var meshes = drawer.Draw(triangle, () => UnityEngine.Color.green);
        return meshes;
    }

    private static List<Mesh> CreateRectangleMap(MapDrawer drawer,Map mapFactory)
    {
        var rectangle = mapFactory.Create(MapType.Parallelogram, new PointF(20,20),
            new Layout()
            {
                Orientation = Orientation.Down,
                Origin = new PointF(-300, -300),
                Size = new PointF(1, 1),
                Type = HexType.Flat
            });

       return drawer.Draw(rectangle, () => UnityEngine.Color.blue);
    }

    private static List<Mesh> CreateHexagonMap(MapDrawer drawer,Map mapFactory)
    {
        var hexagon = mapFactory.Create(MapType.Hexagon, new PointF(10, 0),
            new Layout()
            {
                Orientation = Orientation.Down,
                Origin = new PointF(400, -300),
                Size = new PointF(1, 1),
                Type = HexType.Pointy
            });

        return drawer.Draw(hexagon, () => UnityEngine.Color.black);
    }

    private void CreateGameObjects(GameObject parent,IEnumerable<Mesh> meshes)
    {
        foreach (var mesh in meshes)
        {
            CreateGameObject(parent.transform,mesh);
        }
    }

    private void CreateGameObject(Transform parent,Mesh mesh)
    {
        var go = new GameObject();
        var meshRenderer = go.AddComponent<MeshRenderer>();
        meshRenderer.material = new Material(Shader.Find("Sprites/Default"));
        var filter = go.AddComponent<MeshFilter>();
        filter.mesh = mesh;

        go.transform.SetParent(parent);
    }
}
