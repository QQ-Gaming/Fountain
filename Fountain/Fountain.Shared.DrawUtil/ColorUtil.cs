﻿using System;
using System.Drawing;

namespace Fountain.Shared.DrawUtil
{
    public static class ColorUtil
    {
        private static Random _rand = new Random();


        public static Color Random()
        {
            return Color.FromArgb(_rand.Next(0, 255), _rand.Next(0, 255), _rand.Next(0, 255));
        }

    }
}
