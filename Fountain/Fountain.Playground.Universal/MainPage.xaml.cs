﻿using Fountain.Hexagon;
using Fountain.Playground.Universal.Drawer;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Fountain.Playground.Universal
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private MainViewModel _viewModel;
        public MainPage()
        {
            InitializeComponent();
            DataContext = _viewModel = new MainViewModel();
            foreach (var polygon in _viewModel.UIElements)
            {
                Canvas.Children.Add(polygon);
            }

        }

    }
}
