﻿using Fountain.Hexagon.Geometry;
using Fountain.Hexagon.Maps;
using Fountain.Playground.Universal.Drawer;
using System;
using System.Collections.Generic;
using System.Drawing;
using Windows.UI.Xaml;

namespace Fountain.Playground.Universal
{
    internal class MainViewModel
    {
        private static Random _random = new Random();

        public List<FrameworkElement> UIElements { get; private set; }

        public MainViewModel()
        {
            UIElements = new List<FrameworkElement>();
            //var drawer = new ParallelogramDrawer();
            //var drawer = new TriangleDrawer();
            //var drawer = new HexagonDrawer();
            var drawer = new RectangleDrawer();
            UIElements.AddRange(drawer.CreateMaps());
        }
    }
}
