﻿using System.Drawing;

namespace Fountain.Playground.Universal.Drawer
{
    internal static class ColorExtensions
    {
        internal static Windows.UI.Color ToColor(this Color color)
        {
            return Windows.UI.Color.FromArgb(color.A,
                 color.R, color.G, color.B);
        }
    }
}
