﻿using Fountain.Hexagon.Geometry;
using Fountain.Hexagon.Maps;
using System.Collections.Generic;
using System.Drawing;
using Windows.UI.Xaml.Shapes;

namespace Fountain.Playground.Universal.Drawer
{
    internal class ParallelogramDrawer
    { 
        internal List<Polygon> CreateMaps()
        {
            var polygons = new List<Polygon>();

            polygons.AddRange(CreateMap(new PointF(5, 5), new Layout()
            {
                Orientation = Orientation.Up,
                Origin = new PointF(100, 450),
                Size = new PointF(20, 20),
                Type = Hexagon.HexType.Flat,
            }, Color.Red, Color.Black));

            polygons.AddRange(CreateMap(new PointF(5, 5), new Layout()
            {
                Orientation = Orientation.Left,
                Origin = new PointF(300, 50),
                Size = new PointF(50, 20),
                Type = Hexagon.HexType.Pointy,
            }, Color.Purple, Color.Black));

            polygons.AddRange(CreateMap(new PointF(5, 5), new Layout()
            {
                Orientation = Hexagon.Maps.Orientation.Right,
                Origin = new PointF(700, 400),
                Size = new PointF(25, 25),
                Type = Hexagon.HexType.Flat,
            }, Color.Turquoise, Color.Black));

            polygons.AddRange(CreateMap(new PointF(5, 5), new Layout()
            {
                Orientation = Orientation.Up,
                Origin = new PointF(950, 600),
                Size = new PointF(40, 40),
                Type = Hexagon.HexType.Pointy,
            }, Color.Transparent, Color.Green));

            polygons.AddRange(CreateMap(new PointF(20, 20), new Layout()
            {
                Orientation = Hexagon.Maps.Orientation.Right,
                Origin = new PointF(300, 500),
                Size = new PointF(4, 4),
                Type = Hexagon.HexType.Pointy,
            }, Color.Orange, Color.Black));

            return polygons;
        }

        private List<Polygon> CreateMap(PointF fields, Layout layout, Color fill, Color border)
        {
            var mapFactory = new Map();
            var map = mapFactory.Create(MapType.Parallelogram,fields, layout);
            var drawer = new HexPolygonDrawer();
            return drawer.Draw(map, fill,border);
        }


    }
}
