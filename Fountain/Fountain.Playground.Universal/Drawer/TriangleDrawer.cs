﻿using Fountain.Hexagon;
using Fountain.Hexagon.Geometry;
using Fountain.Hexagon.Maps;
using System.Collections.Generic;
using System.Drawing;
using Windows.UI.Xaml.Shapes;

namespace Fountain.Playground.Universal.Drawer
{
    internal class TriangleDrawer
    {

        internal List<Polygon> CreateMaps()
        {

            var polygons = new List<Polygon>();
            var map1 = CreateMap(6, new Layout()
            {
                Orientation = Orientation.Left,
                Origin = new PointF(200, -50),
                Size = new PointF(50, 50),
                Type = HexType.Flat
            }, Color.Silver, Color.Gold);

            polygons.AddRange(map1);

            var map2 = CreateMap(5, new Layout()
            {
                Orientation = Orientation.Up,
                Origin = new PointF(500, 25),
                Size = new PointF(25, 25),
                Type = HexType.Pointy
            }, Color.Gold, Color.Silver);

            polygons.AddRange(map2);

            var map3 = CreateMap(10, new Layout()
            {
                Orientation = Orientation.Right,
                Origin = new PointF(300, 25),
                Size = new PointF(10, 10),
                Type = HexType.Flat
            }, Color.Red, Color.Black);

            polygons.AddRange(map3);

            var map4 = CreateMap(80, new Layout()
            {
                Orientation = Orientation.Right,
                Origin = new PointF(800, 150),
                Size = new PointF(4, 4),
                Type = HexType.Pointy
            }, Color.White, Color.Blue);

            polygons.AddRange(map4);

            return polygons;
        }

        private List<Polygon> CreateMap(int size, Layout layout, Color fill, Color border)
        {
            var mapFactory = new Map();
            var map = mapFactory.Create(MapType.Triangle, new PointF(size, 0), layout);

            var drawer = new HexPolygonDrawer();
            return drawer.Draw(map, fill, border);
        }
    }
}
