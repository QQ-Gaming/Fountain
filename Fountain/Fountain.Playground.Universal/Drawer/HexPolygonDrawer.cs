﻿using Fountain.Hexagon;
using Fountain.Hexagon.Coordinates;
using Fountain.Hexagon.Drawing;
using System.Collections.Generic;
using System.Drawing;
using Windows.Foundation;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace Fountain.Playground.Universal.Drawer
{
    internal class HexPolygonDrawer
    {
        public Polygon Draw(Hex hex)
        {
            var polygon = new Polygon();
            polygon.Fill = new SolidColorBrush(hex.Fill.ToColor());
            polygon.Stroke = new SolidColorBrush(hex.Border.ToColor());
            foreach (var corner in hex.CornerPoints)
            {
                polygon.Points.Add(new Windows.Foundation.Point(corner.X, corner.Y));
            }
            return polygon;
        }

        public List<Polygon> Draw(Dictionary<CubeCoordinates, Hex> hexes, Color fill, Color border)
        {
            var polygons = new List<Polygon>();

            foreach (var hex in hexes)
            {
                hex.Value.Fill = fill;
                hex.Value.Border = border;
                var polygon = Draw(hex.Value);
                polygons.Add(polygon);
            }
            return polygons;
        }
    }

    internal class HexLineDrawer
    {
        public IEnumerable<Line> Draw(Hex hex)
        {
            for (int i = 0; i < hex.CornerPoints.Length; i++)
            {
                yield return new Line()
                {
                    X1 = hex.CornerPoints[i].X,
                    X2 = hex.CornerPoints[(i == hex.CornerPoints.Length - 1) ? 0 : i + 1].X,
                    Y1 = hex.CornerPoints[i].Y,
                    Y2 = hex.CornerPoints[(i == hex.CornerPoints.Length - 1) ? 0 : i + 1].Y
                };
            }
        }
    }
}

