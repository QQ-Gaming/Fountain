﻿using Fountain.Hexagon.Geometry;
using Fountain.Hexagon.Maps;
using System.Collections.Generic;
using System.Drawing;
using Windows.UI.Xaml.Shapes;

namespace Fountain.Playground.Universal.Drawer
{
    internal class RectangleDrawer
    {
        internal List<Polygon> CreateMaps()
        {
            var maps = new List<Polygon>();

            var map1 = CreateMap(new PointF(10, 20), new Layout()
            {
                Orientation = Orientation.Default,
                Origin = new PointF(200, 50),
                Size = new PointF(10, 10),
                Type = Hexagon.HexType.Pointy
            }, Color.SteelBlue, Color.White);

            maps.AddRange(map1);

            var map2 = CreateMap(new PointF(10, 10), new Layout()
            {
                Orientation = Orientation.Left,
                Origin = new PointF(500, 50),
                Size = new PointF(15, 15),
                Type = Hexagon.HexType.Flat
            }, Color.White, Color.Blue);

            maps.AddRange(map2);


            return maps;
        }

        private List<Polygon> CreateMap(PointF size, Layout layout, Color fill, Color border)
        {
            var mapFactory = new Map();
            var map = mapFactory.Create(MapType.Rectangle,size, layout);

            var drawer = new HexPolygonDrawer();
            return drawer.Draw(map, fill, border);
        }
    }
}
