﻿using Fountain.Hexagon;
using Fountain.Hexagon.Geometry;
using Fountain.Hexagon.Maps;
using System.Collections.Generic;
using System.Drawing;
using Windows.UI.Xaml.Shapes;

namespace Fountain.Playground.Universal.Drawer
{
    internal class HexagonDrawer
    {
        internal List<Polygon> CreateMaps()
        {
            var polygons = new List<Polygon>();
            var map1 = CreateMap(3, new Layout()
            {
                Orientation = Orientation.Left,
                Origin = new PointF(200, 200),
                Size = new PointF(25, 25),
                Type = HexType.Flat
            }, Color.Silver, Color.Gold);

            polygons.AddRange(map1);

            var map2 = CreateMap(20, new Layout()
            {
                Orientation = Orientation.Up,
                Origin = new PointF(650, 500),
                Size = new PointF(5, 5),
                Type = HexType.Pointy
            }, Color.Gold, Color.Silver);

            polygons.AddRange(map2);

            var map3 = CreateMap(7, new Layout()
            {
                Orientation = Orientation.Up,
                Origin = new PointF(200, 500),
                Size = new PointF(10, 10),
                Type = HexType.Pointy
            }, Color.Black, Color.White);

            polygons.AddRange(map3);

            var map4 = CreateMap(30, new Layout()
            {
                Orientation = Orientation.Up,
                Origin = new PointF(1100, 300),
                Size = new PointF(4, 4),
                Type = HexType.Flat
            }, Color.Violet, Color.Black);

            polygons.AddRange(map4);

            return polygons;
        }

        private List<Polygon> CreateMap(int radius, Layout layout, Color fill, Color border)
        {
            var mapFactory = new Map();
            var map = mapFactory.Create(MapType.Hexagon, new PointF(radius,0), layout);
            var drawer = new HexPolygonDrawer();

            var polygons = new List<Polygon>();

            foreach (var hex in map)
            {
                hex.Value.Fill = fill;
                hex.Value.Border = border;
                var polygon = drawer.Draw(hex.Value);
                polygons.Add(polygon);
            }
            return polygons;
        }
    }
}
