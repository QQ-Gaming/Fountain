﻿using Fountain.Hexagon;
using System.Collections.Generic;
using Fountain.Shared.DrawUtil;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Controls;

namespace Fountain.Playground.Universal.Drawer
{
    internal class HexLines
    {
        internal static void DrawHexLines(Canvas canvas, IEnumerable<Hex> hexes)
        {
            foreach (var hex in hexes)
            {
                DrawHex(canvas, hex);
            }
        }

        internal static void DrawHex(Canvas canvas, Hex hex)
        {
            var drawer = new HexLineDrawer();
            var lines = drawer.Draw(hex);
            var randomColor = ColorUtil.Random();
            foreach (var line in lines)
            {
                line.Stroke = new SolidColorBrush(Windows.UI.Color.FromArgb(randomColor.A,
                    randomColor.R, randomColor.G, randomColor.B));
                line.StrokeThickness = 1;
                canvas.Children.Add(line);
            }
        }
    }
}
