﻿using Fountain.Hexagon.Coordinates;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Hexagon.Test
{
    public class CubeCoordinatesTest
    {
        static Random randGen = new Random();

        [Fact]
        internal static void CubeCoordinates_should_ensure_that_they_dont_violate_their_condition()
        {
            Assert.Throws<ArgumentException>(() => new CubeCoordinates(1, 2, 3));
        }

        [Theory]
        [MemberData(nameof(LineData))]
        internal static void Should_Draw_Lines_Between_CubeCoordinates(
            CubeCoordinates from, CubeCoordinates to, int arrayLength)
        {
            var lines = CubeCoordinates.CalculateLine(from, to);
            Assert.True(lines.Length == arrayLength);
        }

        public static IEnumerable<object[]> LineData =>
            new List<object[]>()
            {
                new object[]{new CubeCoordinates(0,0,0), new CubeCoordinates(1,0,-1), 2},
                new object[]{new CubeCoordinates(0,1,-1), new CubeCoordinates(1,0,-1), 2},
                new object[]{new CubeCoordinates(0,1,-1), new CubeCoordinates(1,1,-2), 2},
                new object[]{new CubeCoordinates(0,0,0), new CubeCoordinates(5,0,-5), 6},
            };

        [Theory]
        [MemberData(nameof(ZeroRandDoubleData))]
        internal static void FractionalCubeCoords_should_ensure_that_they_dont_violate_their_condition_positive(
                double x, double y, double z)
        {
            new FractionalCubeCoords(x, y, z);
        }

        [Theory]
        [MemberData(nameof(ZeroRandDoubleData))]
        internal static void FractionalCubeCoords_should_ensure_that_they_dont_violate_their_condition_negative(
                double x, double y, double z)
        {
            Assert.Throws<ArgumentException>(() =>
                new FractionalCubeCoords(x + 0.0000001, y, z));
        }

        [Theory]
        [MemberData(nameof(FractionalCubeCoordsRoundData))]
        internal static void FractionalCubeCoords_can_round_to_nearest_CubeCoords(
                FractionalCubeCoords frac, CubeCoordinates match)
        {
            var rounded = frac.Round();
            Assert.True(rounded == match);
        }

        public static IEnumerable<object[]> FractionalCubeCoordsRoundData =>
            new List<object[]>()
            {
                new object[]{new FractionalCubeCoords(0.50, 0, -0.50), new CubeCoordinates(0, 0, 0)},
                new object[]{new FractionalCubeCoords(0, 0.50, -0.50), new CubeCoordinates(0, 0, 0)},
                new object[]{new FractionalCubeCoords(-0.50, 0, 0.50), new CubeCoordinates(0, 0, 0)},

                new object[]{new FractionalCubeCoords(0.51, 0, -0.51), new CubeCoordinates(1, 0, -1)},
                new object[]{new FractionalCubeCoords(0, 0.51, -0.51), new CubeCoordinates(0, 1, -1)},
                new object[]{new FractionalCubeCoords(-0.51, 0, 0.51), new CubeCoordinates(-1, 0, 1)},

                new object[]{new FractionalCubeCoords(1.50, 0, -1.50), new CubeCoordinates(2, 0, -2)},
                new object[]{new FractionalCubeCoords(0, 1.50, -1.50), new CubeCoordinates(0, 2, -2)},
                new object[]{new FractionalCubeCoords(-1.50, 0, 1.50), new CubeCoordinates(-2, 0, 2)},

                new object[]{new FractionalCubeCoords(1.90, -0.40, -1.50), new CubeCoordinates(2, 0, -2)},
                new object[]{new FractionalCubeCoords(-0.40, 1.90, -1.50), new CubeCoordinates(0, 2, -2)},
                new object[]{new FractionalCubeCoords(-1.90, 0.40, 1.50), new CubeCoordinates(-2, 0, 2)},

                new object[]{new FractionalCubeCoords(1.90, -0.30, -1.60, true), new CubeCoordinates(2, 0, -2)},
                new object[]{new FractionalCubeCoords(-0.30, 1.90, -1.60, true), new CubeCoordinates(0, 2, -2)},
                new object[]{new FractionalCubeCoords(-1.90, 0.30, 1.60, true), new CubeCoordinates(-2, 0, 2)},
            };

        public static IEnumerable<object[]> ZeroRandDoubleData()
        {
            foreach (var item in Enumerable.Range(0, 10))
            {
                var randA = (randGen.NextDouble() - 0.5) * 2;
                var randB = (randGen.NextDouble() - 0.5) * 2;
                var randC = -randA - randB;

                yield return new object[] { randA, randB, randC };
            }
        }
    }
}
