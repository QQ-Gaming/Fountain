﻿using Xunit;

namespace Hexagon.Test
{
    public static class Example
    {
        [Theory]
        [InlineData(4,2,2)]
        [InlineData(4, 3, 1)]
        [InlineData(7, 2, 5)]
        public static void AddTheory(int result,int a,int b)
        {
            Assert.True(result == Add(a, b));
        }

        public static int Add(int a,int b)
        {
            return a + b;
        }
    }
}
