﻿using Fountain.Hexagon;
using Fountain.Hexagon.Coordinates;
using Fountain.Hexagon.Geometry;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Hexagon.Test
{
    public static class RingTest
    {
        public static IEnumerable<object[]> SpiralData => new List<object[]>()
        {
            new object[]{new CubeCoordinates(0,0,0),0, new CubeCoordinates[] {new CubeCoordinates(0,0,0)}},
            new object[]{new CubeCoordinates(0,0,0),1, Neighbors.GetNeighborsWithCenter(new CubeCoordinates(0,0,0))}
        };

        public static IEnumerable<object[]> RingData => new List<object[]>()
        {
            new object[]{new CubeCoordinates(0,0,0),0, new CubeCoordinates[] {new CubeCoordinates(0,0,0)}},
            new object[]{new CubeCoordinates(0,0,0),1, Neighbors.GetNeighbors(new CubeCoordinates(0,0,0))}
        };

        [Theory]
        [MemberData(nameof(SpiralData))]
        public static void Calculate_a_spiral_ring_from_a_radius(CubeCoordinates center, int radius, CubeCoordinates[] expected)
        {
            var result = Ring.Spiral(center, radius);

            Assert.True(expected.All((cube) => result.Contains(cube)) && expected.Length == result.Count());
        }

        [Theory]
        [MemberData(nameof(RingData))]
        public static void Calculate_a_single_ring_from_a_radius(CubeCoordinates center, int radius, CubeCoordinates[] expected)
        {
            var result = Ring.Single(center, radius);

            Assert.True(expected.All((cube) => result.Contains(cube)) && expected.Length == result.Count());
        }


    }
}
