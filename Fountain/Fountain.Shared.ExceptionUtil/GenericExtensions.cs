﻿using System;

namespace Fountain.Shared.ExceptionUtil
{
    public static class GenericExtensions
    {
        public static void Ensure<T, TException>(this T obj, Func<bool> func, TException exception)
            where TException : Exception
        {
            if (!func.Invoke()) throw exception;
        }

        public static void Ensure<T>(this T obj, Func<bool> func)
        {
            if (!func.Invoke()) throw new ArgumentException(nameof(obj));
        }

        public static void Ensure<T>(this T obj, bool isValid)
        {
            if (!isValid) throw new ArgumentException(nameof(obj));
        }
        public static void Ensure<T, TException>(this T obj, bool isValid, TException exception)
            where TException : Exception
        {
            if (!isValid) throw exception;
        }

        public static void NotNull<T>(this T obj)
            where T : class
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));
        }
    }
}
